#ifndef DLIB_VECTOR_H
#define DLIB_VECTOR_H
#pragma once

#include <cmath>
#include <cstdarg>
#include <iostream>

#include "dlib.h"

namespace dlib {
	template<unsigned SIZE, typename TYPE>
	struct vec {
		TYPE vector[SIZE];
		
		vec() {}
		
		template<typename... TYPES>
		vec(TYPE value0, TYPES... values) {
			//std::cout << "Initializing " << value0 << ":" << values << std::endl;
			dlib_vector_initialize(0, value0, values...);
		}
		
		template<typename... TYPES>
		void dlib_vector_initialize(int index, TYPE value0, TYPES... values) {
			vector[index] = value0;
			dlib_vector_initialize(++index, values...);
		}
		
		void dlib_vector_initialize(int index, TYPE value) {
			vector[index] = value;
		}
		
		TYPE& operator[] (const unsigned index) {
			dlib_assert(index < SIZE);
			return vector[index];
		}
	};
	
	template<unsigned SIZE, typename TYPE>
	vec<SIZE, TYPE> operator+ (vec<SIZE, TYPE> vector1, vec<SIZE, TYPE> vector2) {
		vec<SIZE, TYPE> vector0;
		for (unsigned i = 0; i < SIZE; i++)
			vector0[i] = vector1[i] + vector2[i];
		return vector0;
	}
	
	template<unsigned SIZE, typename TYPE>
	vec<SIZE, TYPE> operator- (vec<SIZE, TYPE> vector1, vec<SIZE, TYPE> vector2) {
		vec<SIZE, TYPE> vector0;
		for (unsigned i = 0; i < SIZE; i++)
			vector0[i] = vector1[i] - vector2[i];
		return vector0;
	}
	
	template<unsigned SIZE, typename TYPE>
	vec<SIZE, TYPE> operator* (vec<SIZE, TYPE> vector1, vec<SIZE, TYPE> vector2) {
		vec<SIZE, TYPE> vector0;
		for (unsigned i = 0; i < SIZE; i++)
			vector0[i] = vector1[i] * vector2[i];
		return vector0;
	}
	
	template<unsigned SIZE, typename TYPE, typename SCALAR_TYPE>
	vec<SIZE, TYPE> operator* (vec<SIZE, TYPE> vector, SCALAR_TYPE scalar) {
		vec<SIZE, TYPE> multiply;
		for (unsigned i = 0; i < SIZE; i++)
			multiply[i] = (TYPE) (vector[i] * scalar);
		return multiply;
	}
	
	template<unsigned SIZE, typename TYPE>
	vec<SIZE, TYPE> operator- (vec<SIZE, TYPE> vector) {
		vec<SIZE, TYPE> reverse;
		for (unsigned i = 0; i < SIZE; i++)
			reverse[i] = -(vector[i]);
		return reverse;
	}
	
	template<unsigned SIZE, typename TYPE>
	TYPE length(vec<SIZE, TYPE> vector) {
		TYPE x = 0;
		for (int i = 0; i < SIZE; i++)
			x += vector[i] * vector[i];
		x = std::sqrt(x);
	}
	
	template<unsigned SIZE, typename TYPE>
	vec<SIZE, TYPE> normalize(vec<SIZE, TYPE> vector) {
		TYPE len = length(vector);
		vec<SIZE, TYPE> unit;
		for (int i = 0; i < SIZE; i++)
			unit[i] = vector[i] / len;
		return unit;
	}
	
	template<unsigned SIZE, typename TYPE>
	TYPE dot(vec<SIZE, TYPE> vector1, vec<SIZE, TYPE> vector2) {
		TYPE product = 0;
		for (unsigned i = 0; i < SIZE; i++)
			product += vector1[i] * vector2[i];
		return product;
	}
}

#endif
