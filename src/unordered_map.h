#ifndef DLIB_UNORDERED_MAP_H
#define DLIB_UNORDERED_MAP_H
#pragma once
namespace dlib {
	/*template<typename T>
	struct node_t {
		T element_;
		node_t<T> *next_;
		node_t<T> *prev_;
		int index_;
	};

	template<typename K, typename V>
	class map {== 

		int length_;

		node_t<T> *node_first_;
		node_t<T> *node_last_;
		
		node_t<T> *get_next(int index, struct node_t<T> *node) {
			return (index == node->index_ ? node : get_next(index, node->next_));
		}

		node_t<T> *get_prev(int index, struct node_t<T> *node) {
			return (index == node->index_ ? node : get_prev(index, node->prev_));
		}

		node_t<T> *get_node(int index) {
			if (index >= length_ / 2) {
				return get_prev(index, node_last_);
			} else {
				return get_next(index, node_first_);
			}
		}

	public:

		T get(int index) {
			return get_node(index)->element_;
		}

		T remove(int index) {
			node_t<T> *node = get_node(index);
			node_t<T> *next = node->next_;
			if (index == 0)
				node_first_ = next;
			node_t<T> *prev = node->prev_;
			next->prev_ = prev;
			prev->next_ = next;

			for (int i = index; i < length_ - 1; i++) {
				--next->index_;
				next = next->next_;
			}
			T element = node->element_;
			free(node);
			--length_;
			return element;
		}

		void add(T element) {
			node_t<T> *node = (node_t<T>*) malloc(sizeof(node_t<T>));
			if (length_ == 0)
				node_first_ = node;
			node->prev_ = node_last_;
			node->index_ = length_++;
			node->element_ = element;
			node_last_->next_ = node;
			node_last_ = node;
		}

		void add(int index, T element) {
			node_t<T> *node = (node_t<T>*) malloc(sizeof(node_t<T>));
			if (index == 0)
				node_first_ = node;
			node_t<T> *next = get_node(index);
			node_t<T> *prev = next->prev_;
			node->index_ = index;
			node->element_ = element;
			node->next_ = next;
			node->prev_ = prev;
			next->prev_ = node;
			prev->next_ = node;
			for (int i = index; i < length_ - 1; i++) {
				++next->index_;
				next = next->next_;
			}
		}

		T replace(int index, T element) {
			node_t<T> *node = get_node(index);
			T replaced_element = node->element_;
			node->element_ = element;
			return replaced_element;
		}

		int length() {
			return length_;
		}

		::std::string to_string() {
			::std::ostringstream stream;
			stream << "[";
			node_t<T> *node = node_first_;
			for (int i = 0; i < length_ - 1; i++) {
				stream << node->element_ << ", ";
				node = node->next_;
			}
			stream << node->element_ << "]";
			return stream.str();
		}
	};*/
}
#endif
