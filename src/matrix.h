#ifndef DLIB_MATRIX_H
#define DLIB_MATRIX_H
#pragma once

#include <math.h>

#include "dlib.h"
#include "vector.h"

#define LOG(x) std::cout << x << std::endl
#define IDENTITY(x) initialize(& x, ::dlib::INIT_IDENTITY)
#define BLANK(x) initialize(& x, ::dlib::INIT_BLANK)

#define RADIANS(degrees) ((degrees * M_PI) / 180.0f)
#define DEGREES(radians) ((radians / 180.0f) * M_PI)

namespace dlib {
	
	enum initialization_field {
		INIT_BLANK,
		INIT_IDENTITY
	};
	
	template<unsigned ROWS, unsigned COLUMNS, typename TYPE>
	struct matrix_t;
	
	template<unsigned ROWS, unsigned COLUMNS, typename TYPE>
	struct row_accessor_t {
		unsigned row;
		matrix_t<ROWS, COLUMNS, TYPE> *matrix;
		
		TYPE& operator[] (const unsigned column) {
			return (*matrix)(row, column);
		}
		
		void operator= (TYPE value) {
			if (ROWS != COLUMNS)
				return;
			BLANK(*this);
			for (int i = 0; i < ROWS; i++)
				(*this)[i][i] = value;
		}
	};
	
	template<unsigned ROWS, unsigned COLUMNS, typename TYPE>
	struct matrix_t {
		TYPE matrix[ROWS * COLUMNS] = {0};
		
		TYPE& operator() (const unsigned row, const unsigned column) {
			dlib_assert(row < ROWS && column < COLUMNS);
			return matrix[row * 4 + column];
		}
		
		row_accessor_t<ROWS, COLUMNS, TYPE> operator[] (const unsigned row) {
			row_accessor_t<ROWS, COLUMNS, TYPE> accessor;
			accessor.matrix = this;
			accessor.row = row;
			return accessor;
		}
	};
	
	template<unsigned ROWS, unsigned COLUMNS, typename TYPE>
	vector_t<COLUMNS, TYPE> get_row(matrix_t<ROWS, COLUMNS, TYPE> matrix, unsigned row) {
		vector_t<COLUMNS, TYPE> vector;
		for (unsigned i = 0; i < COLUMNS; i++)
			vector[i] = matrix[row][i];
		return vector;
	}
	
	template<unsigned ROWS, unsigned COLUMNS, typename TYPE>
	vector_t<ROWS, TYPE> get_column(matrix_t<ROWS, COLUMNS, TYPE> matrix, unsigned column) {
		vector_t<ROWS, TYPE> vector;
		for (unsigned i = 0; i < ROWS; i++)
			vector[i] = matrix[i][column];
		return vector;
	}
	
	template<unsigned ROWS, unsigned COLUMNS, typename TYPE>
	void swap_rows(matrix_t<ROWS, COLUMNS, TYPE> *matrix, int a, int b) {
		TYPE x;
		for (unsigned i = 0; i < COLUMNS; i++) {
			x = (*matrix)[a][i];
			(*matrix)[a][i] = (*matrix)[b][i];
			(*matrix)[b][i] = x;
		}
	}
	
	
	
	// operators
	
	template<unsigned ROWS, unsigned COLUMNS, typename TYPE>
	matrix_t<ROWS, COLUMNS, TYPE> operator+ (matrix_t<ROWS, COLUMNS, TYPE> matrix1,
											 matrix_t<ROWS, COLUMNS, TYPE> matrix2) {
		matrix_t<ROWS, COLUMNS, TYPE> matrix0;
		for (unsigned i = 0, j; i < ROWS; i++)
			for (j = 0; j < COLUMNS; j++)
				matrix0[i][j] = matrix1[i][j] + matrix2[i][j];
		return matrix0;
	}
	
	template<unsigned ROWS, unsigned COLUMNS, typename TYPE>
	matrix_t<ROWS, COLUMNS, TYPE> operator- (matrix_t<ROWS, COLUMNS, TYPE> matrix1,
											 matrix_t<ROWS, COLUMNS, TYPE> matrix2) {
		matrix_t<ROWS, COLUMNS, TYPE> matrix0;
		for (unsigned i = 0, j; i < ROWS; i++)
			for (j = 0; j < COLUMNS; j++)
				matrix0[i][j] = matrix1[i][j] - matrix2[i][j];
		return matrix0;
	}
	
	template<unsigned ROWS, unsigned COLUMNS, typename TYPE>
	matrix_t<(ROWS < COLUMNS ? ROWS : COLUMNS), (ROWS < COLUMNS ? ROWS : COLUMNS), TYPE> operator* (
			matrix_t<ROWS, COLUMNS, TYPE> matrix1,
			matrix_t<COLUMNS, ROWS, TYPE> matrix2) {
		if (COLUMNS < ROWS)
			return matrix2 * matrix1;
		
		matrix_t<ROWS < COLUMNS ? ROWS : COLUMNS, ROWS < COLUMNS ? ROWS : COLUMNS, TYPE> matrix0;
		for (unsigned i = 0, j; i < ROWS; i++) {
			vector_t<COLUMNS, TYPE> vec0 = get_row(matrix1, i);
			for (j = 0; j < ROWS; j++) {
				vector_t<COLUMNS, TYPE> vec1 = get_column(matrix2, j);
				matrix0[i][j] = dot(vec0, vec1);
			}
		}
		return matrix0;
	}
	
	template<typename TYPE>
	matrix_t<4, 4, TYPE> rotate(matrix_t<4, 4, TYPE> matrix, TYPE angle, vector_t<3, TYPE> axis) {
		vector_t<3, TYPE> unit_axis = normalize(axis);
		
		TYPE cos = (TYPE) std::cos(angle);
		TYPE sin = (TYPE) std::sin(angle);
		TYPE omc = 1 - cos;
		TYPE x = unit_axis[0];
		TYPE y = unit_axis[1];
		TYPE z = unit_axis[2];
		TYPE xx = x * x;
		TYPE yy = y * y;
		TYPE zz = z * z;
		TYPE xy = x * y;
		TYPE yz = y * z;
		TYPE zx = z * x;
		TYPE sx = x * sin;
		TYPE sy = y * sin;
		TYPE sz = z * sin;
		
		matrix_t<4, 4, TYPE> rotation;
		rotation[0][0] = xx * omc + cos;
		rotation[0][1] = xy * omc - sz;
		rotation[0][2] = zx * omc + sy;
		rotation[1][0] = xy * omc + sz;
		rotation[1][1] = yy * omc + cos;
		rotation[1][2] = yz * omc - sx;
		rotation[2][0] = zx * omc - sy;
		rotation[2][1] = yz * omc + sx;
		rotation[2][2] = zz * omc + cos;
		
		return matrix * rotation;
	}
	
	template<typename TYPE>
	matrix_t<4, 4, TYPE> translate(matrix_t<4, 4, TYPE> src, vector_t<3, TYPE> vector) {
		matrix_t<4, 4, TYPE> dest = src;
		
		TYPE x = vector[0];
		TYPE y = vector[1];
		TYPE z = vector[2];
		
		std::cout << x << ":" << y << ":" << z << std::endl;
		
		dest[3][0] = dest[3][0] + src[0][0] * x + src[1][0] * y + src[2][0] * z;
		dest[3][1] = dest[3][1] + src[0][1] * x + src[1][1] * y + src[2][1] * z;
		dest[3][2] = dest[3][2] + src[0][2] * x + src[1][2] * y + src[2][2] * z;
		dest[3][3] = dest[3][3] + src[0][3] * x + src[1][3] * y + src[2][3] * z;
		
		return dest;
	}
	
	/*template<unsigned ROWS, unsigned COLUMNS, typename TYPE>
	matrix_t<ROWS, COLUMNS, TYPE> inverse(matrix_t<ROWS, COLUMNS, TYPE> src) {
		
		return src;
	}*/
	
	// scalar stuff, very simple
	
	template<unsigned ROWS, unsigned COLUMNS, typename TYPE>
	matrix_t<ROWS, COLUMNS, TYPE> operator+ (matrix_t<ROWS, COLUMNS, TYPE> matrix1, int scalar) {
		
		matrix_t<ROWS, COLUMNS, TYPE> matrix0;
		for (unsigned i = 0, j; i < ROWS; i++)
			for (j = 0; j < COLUMNS; j++)
				matrix0[i][j] = matrix1[i][j] * scalar;
		return matrix0;
	}
	
	template<unsigned ROWS, unsigned COLUMNS, typename TYPE>
	matrix_t<ROWS, COLUMNS, TYPE> operator+ (matrix_t<ROWS, COLUMNS, TYPE> matrix1, float scalar) {
		
		matrix_t<ROWS, COLUMNS, TYPE> matrix0;
		for (unsigned i = 0, j; i < ROWS; i++)
			for (j = 0; j < COLUMNS; j++)
				matrix0[i][j] = matrix1[i][j] * scalar;
		return matrix0;
	}
	
	template<unsigned ROWS, unsigned COLUMNS, typename TYPE>
	matrix_t<ROWS, COLUMNS, TYPE> operator+ (matrix_t<ROWS, COLUMNS, TYPE> matrix1, double scalar) {
		
		matrix_t<ROWS, COLUMNS, TYPE> matrix0;
		for (unsigned i = 0, j; i < ROWS; i++)
			for (j = 0; j < COLUMNS; j++)
				matrix0[i][j] = matrix1[i][j] * scalar;
		return matrix0;
	}
	
	template<unsigned ROWS, unsigned COLUMNS, typename TYPE>
	matrix_t<ROWS, COLUMNS, TYPE> operator+ (int scalar, matrix_t<ROWS, COLUMNS, TYPE> matrix1) {
		return matrix1 + scalar;
	}
	
	template<unsigned ROWS, unsigned COLUMNS, typename TYPE>
	matrix_t<ROWS, COLUMNS, TYPE> operator+ (float scalar, matrix_t<ROWS, COLUMNS, TYPE> matrix1) {
		return matrix1 + scalar;
	}
	
	template<unsigned ROWS, unsigned COLUMNS, typename TYPE>
	matrix_t<ROWS, COLUMNS, TYPE> operator+ (double scalar, matrix_t<ROWS, COLUMNS, TYPE> matrix1) {
		return matrix1 + scalar;
	}
	
	template<unsigned ROWS, unsigned COLUMNS, typename TYPE>
	void initialize(matrix_t<ROWS, COLUMNS, TYPE> *matrix, initialization_field init) {
		switch (init) {
			case INIT_IDENTITY:
				for (int i = 0, j; i < ROWS; i++)
					for (j = 0; j < COLUMNS; j++)
						(*matrix)[i][j] = 
							(i == j ? 1 : 0);
				break;
			case INIT_BLANK:
				for (int i = 0, j; i < ROWS; i++)
					for (j = 0; j < COLUMNS; j++)
						(*matrix)[i][j] = 0;
				break;
			default:
				initialize(matrix, INIT_BLANK);
				break;
		}
	}
}

#endif
