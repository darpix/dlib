#pragma once
#ifndef DLIB_DUAL_MAP_H
#define DLIB_DUAL_MAP_H

#include <unordered_map>

namespace dlib {
	template <typename A, typename B>
	class dual_map {
		
	private:
		std::unordered_map<A, B*> map_a;
		std::unordered_map<B, A*> map_b;
		
		std::unordered_map<A, B> iterate_map;
		
		typedef std::unordered_map<A, B>::iterator iterator;
		typedef std::unordered_map<A, B>::const_iterator const_iterator;
		
	public:
	
		dual_map() {}
		
		int count(A a) {
			return map_a.count(a);
		}
		
		int count(B b) {
			return map_b.count(b);
		}
		
		int count(A a, B b) {
			return count(a) || count(b);
		}
		
		int put(A a, B b) {
			A *loc_a;
			B *loc_b;
			loc_a = &map_a[a];
			loc_b = &map_b[b];
			
			*loc_a = NULL;
			*loc_b = loc_a;
			*loc_a = loc_b;
			return 1;
		}
		
		int remove(A a) {
			B *b = map_a[a];
			map_b.erase(*b);
			map_a.erase(a);
			return 1;
		}
		
		int remove(B b) {
			A *a = map_b[b];
			map_a.erase(*a);
			map_b.erase(b);
			return 1;
		}
		
		B get(A a) {
			return *map_a[a];
		}
		
		A get(B b) {
			return *map_b[b];
		}
		
		void clear() {
			map_a.clear();
			map_b.clear();
		}
		
		iterator begin() {
			for (std::pair<A, B*> pair : map_a)
				iterate_map[pair.first] = *pair.second;
			return iterate_map.begin();
		}

		const_iterator begin() const {
			for (std::pair<A, B*> pair : map_a)
				iterate_map[pair.first] = *pair.second;
			return iterate_map.begin();
		}
		
		iterator end() {
			iterate_map.clear();
			return iterate_map.end();
		}

		const_iterator end() const {
			iterate_map.clear();
			return iterate_map.end();
		}
}

#endif
