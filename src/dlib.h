#ifndef DLIB_DLIB_H
#define DLIB_DLIB_H
#pragma once

#ifdef DLIB_DEBUG
#   include <assert.h>
#   define dlib_assert(val) assert( val )
#else
#   define dlib_assert(val) ((void)0)
#endif

#endif
