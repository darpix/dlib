#include <iostream>

#include <gmtl/Matrix.h>
#include <gmtl/MatrixOps.h>

#include "linked_list.h"
#include "vector.h"
#include "matrix.h"

#define LOG(x) std::cout << x << std::endl

using namespace dlib;
using namespace gmtl;

template<unsigned ROWS, unsigned COLUMNS, typename TYPE>
void print_matrix(matrix_t<ROWS, COLUMNS, TYPE> matrix) {
	for (int i = 0, j; i < ROWS; i++) {
		for (j = 0; j < COLUMNS; j++) {
			std::cout << matrix[i][j] << " ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

template<typename TYPE, unsigned ROWS, unsigned COLUMNS>
void print_matrix(Matrix<TYPE, ROWS, COLUMNS> matrix) {
	for (int i = 0, j; i < ROWS; i++) {
		for (j = 0; j < COLUMNS; j++) {
			std::cout << matrix[i][j] << " ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

int main(int argc, char **argv) {
    dlib::linked_list<int> list;
	
	typedef matrix_t<4, 4, float> mat4;
	typedef matrix_t<3, 4, float> mat3x4;
	typedef matrix_t<4, 3, float> mat4x3;
	typedef matrix_t<3, 3, float> mat3x3;
	
	mat3x4 a;
	mat4x3 b;
	
	BLANK(a);
	BLANK(b);
	
	a[0][0] = 5;
	b[0][0] = 5;
	a[1][2] = 3;
	b[2][1] = 3;
	
	print_matrix(a);
	print_matrix(b);
	
	//mat3x3 c = a * b;
	//mat3x3 inverse_c = inverse(c);
	//print_matrix(c);
	
	/*Matrix44f x, y;
	x[0][0] = 5;
	y[0][0] = 5;
	x[2][3] = 3;
	y[3][2] = 3;
	
	print_matrix(x);
	print_matrix(y);
	
	Matrix44f z = x * y;
	
	print_matrix(z);*/
	
	/*list.add(0);
	list.add(1);
	list.add(2);
	list.add(3);
	list.add(4);
	list.add(5);
	list.add(6);
	LOG("add");
	
	list.remove(5);
	LOG("remove");
	list.replace(2, 5);
	LOG("replace");
	list.add(2, 2);
	LOG("insert");
	LOG(list.get(2));
	
	std::cout << (sizeof(linked_list<int>) + sizeof(linked_list_node_t<int>) * list.length()) << ", "
		<< sizeof(linked_list<int>) << ", "
		<< sizeof(linked_list_node_t<int>) << std::endl;
		
	//ivec2 vec0 = vec2(int, 0, 0);
	//ivec2 vec1 = vec2(int, 0, 0);
	//std::cout <<
		
	std::cout << list.to_string() << std::endl;*/
	return 0;
}
